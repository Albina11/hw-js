// Завдання 5


const books = [{
    name: 'Harry Potter',
    author: 'J.K. Rowling'
  }, {
    name: 'Lord of the rings',
    author: 'J.R.R. Tolkien'
  }, {
    name: 'The witcher',
    author: 'Andrzej Sapkowski'
  }];
  
  const bookToAdd = {
    name: 'Game of thrones',
    author: 'George R. R. Martin'
}

const newBooks = [...books, bookToAdd].map(book => ({...book}));
console.log(newBooks);






// Завдання 6

const employee = {
    name: 'Vitalii',
    surname: 'Klichko'
}


const { name, surname } = employee;

const newEmployee = { name, surname, age: 45, salary: 500000 };

console.log(newEmployee);

// Завдання 7

const array = ['value', () => 'showValue'];

const [value, showValue] = array;

// alert(value); 
// alert(showValue()); 