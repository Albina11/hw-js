// 1. Поясніть своїми словами, як ви розумієте, як працює прототипне наслідування в Javascript
// Javascript використовує прототипне наслідування для реалізації наслідування між об'єктами. 
// Кожен об'єкт в Javascript має свій прототип, який є об'єктом, від якого він наслідує свої властивості. 
// Якщо властивість не знайдена у поточного об'єкта, то вона буде шукатися у його прототипі




// 2. Для чого потрібно викликати super() у конструкторі класу-нащадка?
// Виклик super() у конструкторі класу-нащадка використовується для виклику конструктора класу-батька. 
// Це дозволяє конструктору класу-нащадка використовувати всі методи та властивості класу-батька. 


class Employee {
    constructor(name, age, salary) {
        this._name = name;
        this._age = age;
        this._salary = salary;
    }

    get name() {
        return this._name;
    }

    set name(value) {
        this._name = value;
    }

    get age() {
        return this._age;
    }

    set age(value) {
        this._age = value;
    }

    get salary() {
        return this._salary;
    }

    set salary(value) {
        this._salary = value;
    }
}

