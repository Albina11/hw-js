class Programmer extends Employee {
    constructor(name, age, salary, lang) {
        super(name, age, salary);
        this._lang = lang;
    }

    get lang() {
        return this._lang;
    }

    set lang(value) {
        this._lang = value;
    }

    get salary() {
        return this._salary * 3;
    }
}

const programmer1 = new Programmer("John Doe", 30, 50000, ["JavaScript", "Python"]);
const programmer2 = new Programmer("Jane Doe", 28, 55000, ["Java", "C++"]);

console.log(programmer1);
console.log(programmer2);