// Наведіть кілька прикладів, коли доречно використовувати в коді конструкцію try...catch.
//  ловить помилки та код не зупиняється а працює далі
// якщо користувачі вводять дані в неправильному форматі
// при комунікації з серверами


const books = [
    { 
      author: "Люсі Фолі",
      name: "Список запрошених",
      price: 70 
    }, 
    {
     author: "Сюзанна Кларк",
     name: "Джонатан Стрейндж і м-р Норрелл",
    }, 
    { 
      name: "Дизайн. Книга для недизайнерів.",
      price: 70
    }, 
    { 
      author: "Алан Мур",
      name: "Неономікон",
      price: 70
    }, 
    {
     author: "Террі Пратчетт",
     name: "Рухомі картинки",
     price: 40
    },
    {
     author: "Анґус Гайленд",
     name: "Коти в мистецтві",
    }
  ];

allowedKeys = ['author', 'name', 'price'];

  books.forEach((book) => {

   objectKeys = Object.keys(book);
   console.log(objectKeys.length !==  [ ...new Set( objectKeys.concat(allowedKeys))].length);

    try {
      if (objectKeys.length !==  [ ...new Set( objectKeys.concat(allowedKeys))].length) {
        throw new Error(`Incorrect book object: ${JSON.stringify(book)}`);
      }
      const li = document.createElement("li");
      li.textContent = `${book.author} - ${book.name} - ${book.price} грн`;
      root.appendChild(li);
    } catch (err) {
      console.error(err);
    }
  });