// Поясніть своїми словами, що таке AJAX і чим він корисний при розробці Javascript.
// AJAX озволяє отримувати дані з сервера та оновлювати вміст сторінки без  перезавантаження сторінки.



const filmList = document.getElementById("film-list");

filmList.innerHTML = '<div class="loader"></div>';

fetch("https://ajax.test-danit.com/api/swapi/films")
  .then((response) => {
    if (!response.ok) {
      throw new Error("Сервер не працює");
    }
    
    return response.json();
  })
  .then((data) => {
    filmList.innerHTML = "";
    
    data.forEach((film) => {
      const filmItem = document.createElement("li");
      filmItem.innerHTML = `<h2>Episode ${film.episodeId}: ${film.title}</h2>
                            <p>${film.openingCrawl}</p>
                            <div class="loader"></div>`;
      filmList.appendChild(filmItem);

      Promise.all(film.characters.map((url) => fetch(url)))
        .then((responses) => 
        Promise.all(responses.map((response) => response.json())))
        .then((characters) => {
          filmItem.querySelector(".loader").remove();

          const characterList = document.createElement("ul");
          characters.forEach((character) => {
            const characterItem = document.createElement("li");
            characterItem.textContent = character.name;
            characterList.appendChild(characterItem);
          });

          filmItem.appendChild(characterList);
        })
        .catch((error) => {
          console.log(`Помилка отримання даних: ${error}`);
        });
    });
  })
  .catch((error) => {
    console.log(`Помилка отримання даних фільму: ${error}`);
    filmList.innerHTML = `<p>Не вдалося отримати дані. Будь-ласка спробуйте пізніше.</p>`;
  });
